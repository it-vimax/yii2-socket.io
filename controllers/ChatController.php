<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 28.12.2017
 * Time: 22:50
 */

namespace app\controllers;

use app\models\NewMessageAdded;
use app\models\Messages;
use app\socketEvent\DragBox;
use Yii;
use yii\swiftmailer\Message;
use yii\web\Controller;

class ChatController extends Controller
{
    public function actionIndex()
    {
        $messages = Messages::find()->all();
        return $this->render('index', compact('messages'));
    }

    public function actionDragBox()
    {
        (new DragBox([
            'pageX' => Yii::$app->request->get('pageX'),
            'pageY' => Yii::$app->request->get('pageY')
        ]))->toOthers()->broadcast(); // срабатывает событие
        return Yii::$app->response->setStatusCode(200);
//        return $this->asJson([
//            'response' => 'AJAX',
//            'pageX' => Yii::$app->request->get('pageX'),
//            'pageY' => Yii::$app->request->get('pageY'),
//        ]);
    }

    public function actionPostMessage()
    {
        $mes = Yii::$app->request->get();
        $message = new Messages();
        $message->user = $mes['user'];
        $message->message = $mes['message'];
        $message->save();
        (new NewMessageAdded([
            'message' => $message->message,
            'user' => $message->user,
            'id' => $message->id,
        ]))->toOthers()->broadcast(); // срабатывает событие
        return $this->goBack();
    }
}