$( function() {
    $( "#draggable" ).draggable();
    $( "#draggable" ).on('drag', (event, ui)=>
    {
        $coordinates = {
            pageX : event.pageX,
            pageY : event.pageY
        };
        // console.log($coordinates);
        $.ajax({
            url: '/chat/drag-box',                 // указываем URL
        	//headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
            // dataType : "json",	        // тип данных возвращаемых в callback функцию (xml, html, script, json, text, _default)
        	data : $coordinates,	            //	даные, которые передаем
        	async : true,	            //	асинхронность запроса, по умолчанию true
        	cache : true,	            //	вкл/выкл кэширование данных браузером, по умолчанию true
        	contentType : "application/x-www-form-urlencoded",
        	type : "GET",            // GET либо POST
        	
            success: function (data)
        	{	// вешаем свой обработчик на функцию success
                // if(!data)
                // {
        	     //    console.warn('false');
        	     //    return;
                // }
                // console.log(data);
            },
        	
        	error: function (error)
        	{	// вешаем свой обработчик на функцию error
        		// console.log(error);
        	},
        	
        	beforeSend: function(){},	//	срабатывает перед отправкой запроса
        	complete: function(){}		//	срабатывает по окончанию запроса
        });
    });

    let socket = io(':6001');
    socket.on('private-box:drag', (data)=>
    {
        // console.log(data);
        $( "#draggable" ).offset({
            top: data.pageY,
            left: data.pageX
        });
    });
} );