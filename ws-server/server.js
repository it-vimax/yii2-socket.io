let io = require('socket.io')(6001),
    Redis = require('ioredis'),
    redis = new Redis();

redis.psubscribe('*', (error, count)=>
{
    //отправка событий

});

// событие перетаскивание кубика
redis.on('pmessage', (pattern, channel, message)=>
{
    message = JSON.parse(message);
    console.log(channel + ':' + message.event + '==[pageX:' + message.data.pageX + ', pageY:' + message.data.pageY + ']');
    io.emit(channel+':'+message.event, message.data);
});
