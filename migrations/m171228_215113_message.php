<?php

use yii\db\Migration;

/**
 * Class m171228_215113_message
 */
class m171228_215113_message extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('messages', [
            'id' => $this->primaryKey(),
            'user' => $this->string(),
            'message' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171228_215113_message cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171228_215113_message cannot be reverted.\n";

        return false;
    }
    */
}
